import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  Objeto1 = {
    nombre: "Checha",
    raza: "Criolla",
    edad: "3 años"
  };

  Objeto2 = {
    nombre: "Kiano",
    raza: "Buldog",
    edad: "1 año"
  }
  
  constructor() { }

  ngOnInit(): void {
  }
}
